/obj/item/clothing/suit/space/bobafett_suit
	name = "bobafett suit"
	desc = "A long time ago..."
	icon_state = "bobafett_suit"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	armor = list(melee = 30, bullet = 30, laser = 30,energy = 30, bomb = 30, bio = 20, rad = 10)
	item_state = "bl_suit"